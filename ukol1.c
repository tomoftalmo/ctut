#include <stdio.h>

// napis funkci hledej, ktera bude mit tri parametry
//  - pole celych cisel
//  - pocet polozek v poli
//  - hledane cislo
// ta projde pole podle zadaneho poctu polozek a pokazde
//  - kdyz najde hledane cislo, tak vypise "jupi :)"
//  - kdyz najde jine cislo, vypise "ou :("
void hledej()
{
    // ??
}


int main()
{

    int pole[] = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3};

    // takze kdyz se zavola takto
    hledej(pole, 10, 3);
    // tak vypise
    // jupi :)
    // ou :(
    // ou :(
    // ou :(
    // ou :(
    // ou :(
    // ou :(
    // ou :(
    // ou :(
    // jupi :)

    printf("\n");

    // a pokud takto
    hledej(pole, 3, 1);
    // tak vypise
    // ou :(
    // jupi :)
    // ou :(

    printf("\n");

    // potom tu funkci uprav, aby (navic) vracela cislo, kolikrat bylo hledane cislo nalezeno
    // takze po uprave s pocitanim
    // by se nakonec vypsalo:
    // 2 1
    // printf("%d %d\n", hledej(pole, 4, 1), hledej(pole, 3, 3));

    return 0;
}
