#include <stdio.h>

typedef struct strom
{
	struct strom*levy;
	struct strom*pravy;
	int data;
}strom;

int vypis(strom*koren)
{
	if (koren!=NULL)
	{
		vypis(koren->levy);
		printf("%d \n",koren->data);
		vypis(koren->pravy);
	}
}

int vloz(strom ** koren,int cislo)
{
	while(*koren != NULL)
	{
		if(cislo < (*koren)->data)
		{
			koren = & (*koren)->levy;
		}
		else 
		{
			koren = & (*koren)->pravy;
		}
	}

	(*koren) = malloc(sizeof(strom));
	if( (*koren) != NULL)
	{
		(*koren)->data=cislo;
		(*koren)->levy=NULL;
		(*koren)->pravy=NULL;
	}
}


int main()
{
	strom *s = NULL;

	vloz(&s, 3);
	vloz(&s, 9);
	vloz(&s, 1);

	vypis(s);

	return 0;
}