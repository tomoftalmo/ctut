#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 NAME"
    exit 1
fi

echo "$1.c" | entr -c -- /bin/bash -c "make \"$1\" && ./\"$1\""
