#include <stdio.h>
#include <math.h>
#include <stdlib.h>

// float sqrt(float)

float cislo()
{
	int zasah=0;
	int celkem = 100000;

	for(int i=0;i<celkem;i++)
	{
		float x=(float) rand()/RAND_MAX;
		float y=(float) rand()/RAND_MAX;
		if(sqrt(x*x+y*y)<1)
		{
			zasah=zasah+1;
		}
	}
	float p=4.0 * zasah / celkem;
	return p;
}

int main()
{
	float f = cislo();
	printf("%f \n", f);
}