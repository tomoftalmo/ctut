// binarni vyhledavaci strom

#include <stdio.h>
#include <stdlib.h>

// definice struktury pro jeden uzel stromu (node = uzel)
// obsahuje data a ukazatele na levy a pravy podstrom
struct node {
    int data;
    struct node *left;
    struct node *right;
};


// tohoto si nevsimej, takto se to nedela :D
struct node nodes[] = {
    {15, &nodes[1], &nodes[2]},
    {8, &nodes[3], &nodes[4]},
    {20, &nodes[5], &nodes[6]},
    {3, &nodes[7], &nodes[8]},
    {12, &nodes[9], NULL},
    {16, NULL, NULL},
    {23, NULL, NULL},
    {1, NULL, NULL},
    {5, NULL, NULL},
    {10, NULL, NULL},
};
// ale vyrobil jsem tim taktovyto strom:
//            15
//        /       \
//      8          20
//    /   \      /   \
//   3     12    16  23
//  / \   /
// 1   5  10
//
// vsimni si, ze pro kazdy uzel plati, ze v jeho levem podstromu jsou jen mensi cisla a v pravem jen vetsi
// to je to, co dela strom vyhledavacim (ta cisla jsou v nem vlastne serazena)


// funce pro vypis stromu
// funguje rekurzivne a vypisuje tzv. in-order, tzn nejdriv vypise levy podstrom, potom sebe a nakonec pravy podstrom
int print_tree(struct node *root) {
    if(root != NULL) {
        print_tree(root->left);
        printf("%d ", root->data);
        print_tree(root->right);
    }
    return 0;
}


// konecne ukol:
// napsat funkci, ktera bude mit jako parametr ukazatel na koren stromu a cislo, ktere v nem bude hledat
// vrati 1, pokud to cislo najde, jinak 0
int hledej() {
    return 0;
}
// napoveda: hleda se podobne, jako jsme meli to vyhledavani v serazenem poli, akorat tady nepocitas
// poloviny intervalu, ale hledas v levem, resp. pravem podstromu podle porovnani hledaneho cisla a dat v uzlu


int main() {

    // koren stromu
    struct node *root = &nodes[0];

    // vypis stromu
    print_tree(root);

    putchar('\n');

    // zkouska hledaci funkce, zkusi ve strome najit cisla od 0 do 24
    // budes muset doplnit parametry k volani hledej() az tu funci upravis
    for(int i = 0; i < 25; ++i) {
        printf("%2d: %d\n", i, hledej());
    }

    return 0;
}
