#include <stdio.h>
#include <math.h>


// funkce pocita kvadratickou rovnici
// a * x^2 + b * x + c = 0
// vysledky uklada do promennych x1, x2 predanych ukazatelem
// vraci pocet reseni
int kvadraticka_rovnice(float a, float b, float c, float *x1, float *x2) {
    int reseni;
    if(a == 0) {
        // neni kvadraticka rovnice
        return 0;
    }
    else {
        float d = b * b - 4.0 * a * c;
        if(d < 0) {
            // nema realne reseni
            return 0;
        } else if (d == 0) {
            reseni = 1;
        } else {
            reseni = 2;
        }
        *x1 = (-b + sqrt(d)) / 2.0 / a;
        *x2 = (-b - sqrt(d)) / 2.0 / a;
    }
    return reseni;
}


int main()
{
    // tvym ukolem je doplnit main tak, abys spocitala koreny rovnice
    // 2x^2 + 3x - 12 = 0
    // a vypsala je

    return 0;
}
