// tentokrat zase cele sama
// tvym ukolem bude napsat funkci, ktera jako parametr dostane nazev souboru
// tento soubor cely precte a vrati 1, pokud je v nem stejny pocet zavorek '(' a ')'
// jinak vrati 0
// ostatni znaky muzes ignorovat, jde jen o zavorky
//
// priklady:
// ( )    -> 1
// ))((   -> 1
// )      -> 0
// ())    -> 0
//        -> 1 (prazdny soubor)


// pokud to chces mit uplne uzasne spravne tak:
//  - otevreni souboru se nemusi vzdycky podarit a mela bys to kontrolovat
//  - soubor bys mela vzdycky zavrit, az ho nepotrebujes



// potom tu funkci uprav (nebo klidne udelej druhou podobnou), ktera ty zavorky bude
// nejen pocitat, ale taky bude kontrolovat, aby zavorky byly spravne sparovane, takze
// pro kazdou zaviraci zavorku ')' musi existovat predchozi oteviraci '('
// a zase, pokud je to spravne vrati 1, jinak 0
//
// priklady:
// ( )    -> 1
// ))((   -> 0 (protoze jsou nejdriv zaviraci zavorky)
// )      -> 0 (protoze je zaviraci navic)
// ())    -> 0 (protoze je zaviraci navic)
//        -> 1 (prazdny soubor)
// ())(   -> 0 (protoze je zase zaviraci driv nez oteviraci)
// ( (  ) -> 0 (protoze chybi zaviraci zavorka)



// nakonec jako bonus muzes zkusit udelat treti variantu funkce, ktera bude pocitat a parovat zavorky
// jako predchozi verze, akorat navic se v souboru muzou vyskytovat i smajlici :) a :(
// smajlik je smajlikem pouze tehdy, pokud by jeho zavorka byla navic, ale pokud je potreba do paru s obycejnou zavorkou,
// tak to neni smajlik a povazuje se proste za danou zavorku
// a zase, pokud je vse spravne vrati 1, jinak 0
// tohle jsem asi nezvladl uplne dobre vysvetlit, tak si s tim nelam hlavu, kdyby to neslo
//
// priklady:
// )      -> 0 protoze chybi oteviraci zavorka
// :)     -> 1 protoze je to smajlik a ne zavorka, takze pocet zavorek je spravne
// :( )   -> 1 protoze to neni smajlik a pocet zavorek je tedy spravne
// (( :( ) ) ) :) -> 1 je tam sice vice zaviracich zavorek, ale jedna z toho je smajlik, takze to sedi



// napoveda: pokud by se ti nechtelo moc premyslet, jak poznat, ze jsou spravne sparovane zavorky..
// ..staci kontrolovat, ze pocet zaviracich zavorek v zadnem okamziku neni vyssi nez pocet oteviracich
// a samozrejme nakonec musi byt obou stejny pocet
