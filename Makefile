CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra

.PHONY: all

all: $(patsubst %.c,%,$(wildcard *.c))

shello: shello.s
	nasm -felf $<
	# ld $@.o -o $@
	gcc $@.o -o $@
