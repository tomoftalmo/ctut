global main
extern printf

section .text
main:

    ; printf(format, message, 42);
    mov edi, format
    mov esi, message
    mov edx, 42
    xor eax, eax
    call printf

    ; return 0
    mov eax, 1
    mov ebx, 0
    int 80h


section .data

; char format[] = "%s\n";
format:
    db "%s %d", 10, 0

; char message[] = "It works\n";
message:
    db "It works!", 10, 0

