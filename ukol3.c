#include <stdio.h>

struct S {
    int a;
    int b;
};

struct T {
    int a;
    char b;
};

int main() {

    // sizeof je specialni operator, ktery rika, jak velky je datovy typ
    // tzn. kolik bajtu je potreba v pameti na jejich ulozeni

    // napr. sizeof(char) je vzdycky 1
    printf("char: %ld\n", sizeof(char));

    // a neni potreba pouzivat nazev typu, ale klidne i promennou
    char c;
    printf("c: %ld\n", sizeof(c));

    // nebo dokonce libovolny vyraz
    printf("1 + 2: %ld\n", sizeof(1 + 2));


    // pro dalsi typy uz jejich velikost neni presne definovana
    // a zalezi, jaky mas pocitac (procesor)
    // napriklad sizeof(int) by melo byt myslim alespon 2
    // ale nejspis to bude 4

    // tvym ukolem je zjistit a vypsat, kolik mista v pameti
    // zabiraji nasledujici datove typy:
    //   int
    //   ukazatel na int
    //   short int
    //   long int
    //   ukazatel na long int
    //   long long int
    //   float
    //   double
    printf("short: %ld\n", sizeof(short int));

    // vsimni si, ze nektere typy jsou stejne velke a muzou vypadat, ze jsou tam zbytecne navic
    // a jsou vsechny ukazatele stejne?


    // dalsi vec je, ze konstanty zapsane v kodu maji taky svuj typ
    // napr. cela cisla jsou typu int
    // takze sizeof(1) by melo byt stejne jako sizeof(int)

    // takze dalsi ukol: zjistit jakeho typu (podle sizeof) jsou:
    //   desetinne cislo - treba 2.4
    //   F-desetinne cislo - treba 2.4f
    //   znak - treba 'A'

    // znas rozdil mezi "A" a 'A'?


    // bonus: (pokud neznas struktury, tak asi muzes preskocict)
    // zamyslet se proc sizeof(struct T) neni 5, viz nahore
    printf("S: %ld\n", sizeof(struct S));
    printf("T: %ld\n", sizeof(struct T));
    // pokud nemas tuseni, vubec nevadi, to je (zatim) spis zajimavost

    // bonus2: (uz zadne programovani, jen zajimavost)
    // pokud k ukazateli prictu 1, tak se jeho hodnota (adresa) zvysi prave
    // o velikost typu na ktery ukazuje
    int * pi = NULL;
    double * pd = NULL;
    // NULL je specialni hodnota pro ukazatele, je rovna nule
    // a znamena, ze ten ukazatel neukazuje na platnou adresu v pameti

    printf("pi: %d\n", pi);
    printf("pi+1: %d\n", pi+1);
    printf("pd: %d\n", pd);
    printf("pd+1: %d\n", pd+1);

    return 0;
}
