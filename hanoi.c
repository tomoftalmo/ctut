#include <stdio.h>


int hanoi(char a, char b, char c, int n) {
	if(n > 0) {
		hanoi(a, c, b, n-1);
		printf("%c -> %c\n", a, b);
		hanoi(c, b, a, n-1);
	}	
}


int main()
{
	hanoi('A', 'B', 'C', 12);
	return 0;
}